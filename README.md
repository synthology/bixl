# PWA Template

This template removes the need to recreate the PWA boilerplate code all the time. It takes care of the basic PWA requirements, such as offline caching using a service worker, and contains several useful UI features to get you going.

You can start using the template by placing your app code into the `modules/app.js` file. You can add your app's css code in `resources/app.css`.

To customize your app's appearance, you can add your own theme. Look at the theming section of this documentation.

*Folder Structure*
```ascii
pwa-template/
├── modules/
│   └── app.js                          // your app specific code goes here
├── resources/                          // you can use this folder to organise your app resource files
│   ├── fonts/                          // folder that contains font-definitions
│   │   ├── archivo/
│   │   ├── feather/
│   │   ├── lora/
│   │   ├── roboto/
│   │   ├── source-code-pro/
│   │   ├── archivo.css
│   │   ├── feather.css
│   │   ├── lora.css
│   │   ├── roboto.css
│   │   └── source-code-pro.css
│   ├── icons/
│   │   ├── app-icon.png                // this icon will be used for the pwa
│   │   └── app-icon.svg                // an svg template for creating your app icon
│   ├── app.css                         // your app specific css goes here
│   ├── base.css                        // contains the base css for the pwa template functions
│   ├── theme-carbon.css                // theme file for defining colors etc.
│   └── theme.css                       // css file containing the theme framework, this should not be modified
├── base.js                             // the base code for the pwa-template functions
├── index.html                          // app entry point
├── manifest.json                       // manifest file for defining pwa properties
├── offline.js                          // service worker for caching resources offline
└── README.md                           // this file (documentation)
```

## App Features

### Enable Offline Cache

You can enable/disable installing the service worker by adding the `require-offline` attribute to the html tag.
This will allow the app to be available offline. This is enabled by default in the template.

*Example*
```html
<html require-offline>
</html>
```

## UI Features

### Show Message

You can show a blocking message by calling the `message` function from javascript. To use an icon you have to pass a unicode character for the included `feather` icon font. See the 'Icons' section for more info.
Calling the function without parameters will hide the message.

*Syntax*
```javascript
message(text /* optional */, icon /* optional */);
```

*Example*
```javascript
//
// EXAMPLE
//      Show Blocking Message
//
window.setTimeout(function() { message('This is a blocking message.'); }, 1000);
window.setTimeout(function() { message(); }, 2000);

//
// EXAMPLE
//      Show Blocking Message With Icon
//
window.setTimeout(function() { message('This is a blocking message with icon.', '\ue884'); }, 1000);
window.setTimeout(function() { message(); }, 2000);
```

### Show Progress Bar

You can show a progress bar by calling the `progress` function from javascript. You can also add a message or icon. To use an icon you have to pass a unicode character for the included `feather` icon font. See the 'Icons' section for more info.
Calling the function without parameters will hide the progress bar.

*Syntax*
```javascript
progress(percent /* optional */, text /* optional */, icon /* optional */);
```

*Example*
```javascript
//
// EXAMPLE
//      Show Blocking Progress
//
let p = 0;
function update() 
{ 
    // display icon
    progress(p, null, '\ue884');
    // display a message
    // progress(p, 'Downloading...'); 
    p = p + 5;
    if (p<=100)
        window.setTimeout(update, 200)
}
window.setTimeout(update, 1000);
window.setTimeout(progress, 6000);
```

### Start App Blocked

You can add the `block` attribute the the body element. If you display a progressbar or message at the start of your app, this setting will prevent the in-animation of the message/progress bar, and prevents the page from momentarily being displayed while the browser is loading the base assets and scripts.

*Example*
```html
<body block>
</body>
```

### Remove App Block

Removes the page block previously added by setting the `block` attribute on the body element. See 'Start App Blocked' for more info. It is common practice to display progress or a message. When hiding the progress/message, the page will be unblocked automatically, removing the need to call this function.

*Example*
```javascript
//
// EXAMPLE
//      Unblock App
window.setTimeout(unblock, 1000);
```

### Icons

This PWA template uses the open `feather` icon library. You can use these icons anywhere in html by adding the `feather` class to any html element, including the specific class `icon-name` for the icon you want to use.

*Example*
```html
<i class="feather icon-alert-octagon"></i>
```

If you want to use icons in css, the message or progress function, you have to look up the unicode character for the icon in the `resources/fonts/font-feather.css`.

*Example CSS*
```css
div {
    font-family : 'feather';
    content     : '\e81b';
}
```

*Example Javascript*
```javascript
message('This is a mesage.', '\ue81b');
```


### Themes

To create your own theme: 
* Create a new theme file, example: `resources/theme-mycustomtheme.css`, and add the theme properties. See the 'Theme File Description' section for more info.
* Add this theme to the `index.html` file's head section.
    ```html
    <link href="./resources/theme-mycustomtheme.css" rel="stylesheet">
    ```
* Use your theme by setting the `theme` attribute on the html document element.
    ```html
    <html require-javascript require-css-custom-properties require-offline theme="mycustomtheme">
    ```

#### Theme File Description
```css
/* define the theme name */
[theme='mycustomtheme'] {
    /* the default font used for UI stuff */
    --font-family               : 'roboto', sans-serif;
    
    /* default colors used for UI stuff */
    --color                     : rgb(35,   37,  45);
    --background-color          : rgb(250, 252, 255);
}
```