let app = {};

//
// EXAMPLE
//      Show Blocking Message
//
// window.setTimeout(function() { message('This is a blocking message.'); }, 1000);
// window.setTimeout(function() { message(); }, 2000);

//
// EXAMPLE
//      Show Blocking Message With Icon
//
// window.setTimeout(function() { message('This is a blocking message with icon.', '\ue884'); }, 1000);
// window.setTimeout(function() { message(); }, 2000);

//
// EXAMPLE
//      Show Blocking Progress
//
// let p = 0;
// function update() 
// { 
//     // display icon
//     progress(p, null, '\ue884');
//     // display a message
//     // progress(p, 'Downloading...'); 
//     p = p + 5;
//     if (p<=100)
//         window.setTimeout(update, 200)
// }
// window.setTimeout(update, 1000);
// window.setTimeout(progress, 6000);

//
// EXAMPLE
//      Unblock App
// window.setTimeout(unblock, 1000);

export default app;