// TODO - CLEANUP
function dragElement(elmnt) {
    var pos1 = 0, pos2 = 0, pos3 = 0, pos4 = 0;
    if (document.getElementById(elmnt.id + "header")) {
      // if present, the header is where you move the DIV from:
      document.getElementById(elmnt.id + "header").onmousedown = dragMouseDown;
    } else {
      // otherwise, move the DIV from anywhere inside the DIV:
      elmnt.onmousedown = dragMouseDown;
    }
  
    function dragMouseDown(e) {
      e = e || window.event;
      e.preventDefault();
      // get the mouse cursor position at startup:
      pos3 = e.clientX;
      pos4 = e.clientY;
      document.onmouseup = closeDragElement;
      // call a function whenever the cursor moves:
      document.onmousemove = elementDrag;
    }
  
    function elementDrag(e) {
      e = e || window.event;
      e.preventDefault();
      // calculate the new cursor position:
      pos1 = pos3 - e.clientX;
      pos2 = pos4 - e.clientY;
      pos3 = e.clientX;
      pos4 = e.clientY;
      // set the element's new position:
      //elmnt.style.top = (elmnt.offsetTop - pos2) + "px";
      //elmnt.style.left = (elmnt.offsetLeft - pos1) + "px";
      elmnt.style.setProperty('--offset-width', elmnt.offsetWidth);// + 'px');
      elmnt.style.setProperty('--offset-height', elmnt.offsetHeight);// + 'px');
      elmnt.style.setProperty('--offset-top', (elmnt.offsetTop - pos2));// + 'px');
      elmnt.style.setProperty('--offset-left', (elmnt.offsetLeft - pos1));// + 'px');
    }
  
    function closeDragElement() {
      // stop moving when mouse button is released:
      document.onmouseup = null;
      document.onmousemove = null;
    }
}

//
// adds some CSS custom properties to the body tag for help with calculations
//
function updateCSSGlobals() {
    let s = document.documentElement.style;
    s.setProperty('--viewport-width', document.documentElement.offsetWidth);// + 'px');
    s.setProperty('--viewport-height', document.documentElement.offsetHeight);// + 'px');
}

//
// base component
//      Inherited by all other components
//
export class Base extends HTMLElement {
    constructor() {
        super();

        this.$path = import.meta.url.substring(0, import.meta.url.lastIndexOf("/"));

        let baseTemplate = ' \
<link href="' + this.$path + '/base.css" rel="stylesheet"> \
                           ';

        this.$template = document.createElement('template');
        this.$template.innerHTML = this.template + baseTemplate;

        this._shadowRoot = this.attachShadow({ mode: 'open' });
        this._shadowRoot.appendChild(this.$template.content.cloneNode(true));
        this.$control = this._shadowRoot.querySelector('#control');

        this.$attributes = [];
        for (let a in Base.styleAttributes) {
            this.$attributes.push(
                a.replace(
                    /([-_][a-z1-9])/g,
                    (group) => group.toUpperCase()
                                    .replace('-', '')
                                    .replace('_', '')
                )
            );
        }
    }
    get color() {
        return this.getAttribute('color');
    }
    set color(value) {
        this.setAttribute('color', value);
    }
    get opaque() {
        return this.getAttribute('opaque');
    }
    set opaque(value) {
        this.setAttribute('opaque', value);
    }
    get borderAuto() {
        return this.getAttribute('border-auto');
    }
    set borderAuto(value) {
        this.setAttribute('border-auto', value);
    }
    get borderWhite() {
        return this.getAttribute('border-white');
    }
    set borderWhite(value) {
        this.setAttribute('border-white', value);
    }
    get borderBlack() {
        return this.getAttribute('border-black');
    }
    set borderBlack(value) {
        this.setAttribute('border-black', value);
    }
    get borderNone() {
        return this.getAttribute('border-none');
    }
    set borderNone(value) {
        this.setAttribute('border-none', value);
    }
    get backgroundLight() {
        return this.getAttribute('background-light');
    }
    set backgroundLight(value) {
        this.setAttribute('background-light', value);
    }
    get backgroundDark() {
        return this.getAttribute('background-dark');
    }
    set backgroundDark(value) {
        this.setAttribute('background-dark', value);
    }
    get foregroundLight() {
        return this.getAttribute('foreground-light');
    }
    set foregroundLight(value) {
        this.setAttribute('foreground-light', value);
    }
    get foregroundDark() {
        return this.getAttribute('foreground-dark');
    }
    set foregroundDark(value) {
        this.setAttribute('foreground-dark', value);
    }
    get movable() {
        return this.getAttribute('movable');
    }
    set movable(value) {
        this.setAttribute('movable', value);
    }
    static get observedAttributes() {
        let attrs = [
            'movable'
        ];
        for (let s in Base.styleAttributes) {
            attrs.push(s);
        }
        return attrs;
    }
    static get styleAttributes() {
        return {
            'color' : [ 
                'black', 
                'dark-grey', 
                'light-grey', 
                'white', 
                'slate', 
                'grape', 
                'blue', 
                'ocean',
                'mint', 
                'green', 
                'pea', 
                'army', 
                'yellow', 
                'orange', 
                'red', 
                'pink', 
                'purple', 
                'wine', 
                'rosy', 
                'brown' 
            ],
            'opaque' : null, 
            'border-auto' : null, 
            'border-white' : null, 
            'border-black' : null, 
            'border-none' : null, 
            'background-light' : null, 
            'background-dark' : null,
            'foreground-light' : null, 
            'foreground-dark' : null
        };
    }
    attributeChangedCallback(name, oldVal, newVal) {
        this.render();
    }
    render() {
        let attrs = this.$attributes;
        for (let o in attrs) {
            let aName = attrs[o];
            let aReal = Object.keys(Base.styleAttributes)[o];
            let aValue = this[aName];
            if (typeof aValue === 'undefined' ||
                aValue === null)
            {
                // inherit from parent
                let e = this.parentNode.closest('[' + aReal + ']');
                if (e)
                    aValue = e.getAttribute(aReal);
            }

            if (typeof aValue !== 'undefined' &&
                aValue !== null)
            {
                this.$control.setAttribute(aReal, aValue);
            }
        }

        if (this.movable !== null) {
            this.$control.setAttribute('movable', '');
            dragElement(this.$control);
        }
    }
}

// set window listeners for CSS ustom properties
window.onresize  = updateCSSGlobals;

// initial update css custom properties
updateCSSGlobals();