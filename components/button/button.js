import {Base} from '../base/base.js';

const TAG = 'ui-button';

class Button extends Base {
    constructor() {
        super();

        this.$control.addEventListener('click', () => {
            console.log('click');
        });
        this.$icon = this._shadowRoot.querySelector('#icon');
        this.$label = this._shadowRoot.querySelector('#label');
    }
    get template() {
        let path = import.meta.url.substring(0, import.meta.url.lastIndexOf("/"));
        return '\
<div id="control"> \
    <i id="icon"></i><span id="label"><span> \
</div> \
<link href="' + path + '/button.css" rel="stylesheet"> \
<link href="' + path + '/../../resources/fonts/feather.css" rel="stylesheet"> \
        ';
    }
    get icon() {
        return this.getAttribute('icon');
    }
    set icon(value) {
        this.setAttribute('icon', value);
    }
    get label() {
        return this.getAttribute('label');
    }
    set label(value) {
        this.setAttribute('label', value);
    }
    static get observedAttributes() {
        let attrs = [
            'icon',
            'label'
        ];
        attrs.push.apply(attrs, super.observedAttributes);

        return attrs;
    }
    render() {
        super.render();

        if (this.icon) {
            this.$icon.classList.add('feather', 'icon-' + this.icon);
            if (!this.label) {
                this.$icon.classList.add('no-label');
            }
        }
        else {
            this.$icon.className = ''; 
        }

        this.$label.innerHTML = this.label;
    }
}

window.customElements.define(TAG, Button);