import {Base} from '../base/base.js';

const TAG = 'ui-panel';

class Panel extends Base {
    constructor() {
        super();
    }
    get template() {
        let path = import.meta.url.substring(0, import.meta.url.lastIndexOf("/"));
        return '\
<div id="control"> \
<slot></slot> \
</div> \
<link href="' + path + '/panel.css" rel="stylesheet"> \
        ';
    }
    static get observedAttributes() {
        let attrs = [
        ];
        attrs.push.apply(attrs, super.observedAttributes);

        return attrs;
    }
    render() {
        super.render();
    }
}

window.customElements.define(TAG, Panel);

//WHERE WAS I -> MAKE MOVABLE ELEMENT DEFINABLE, SO THAT YOU CAN SPECIFY IF
//               THE ENTIRE PANEL IS CLICKADBLE, OR JUST AN ELEMENT, SUCH AS A TITLEBAR